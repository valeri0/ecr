## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.4.0 |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ecr_lifecycle_policy.policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_lifecycle_policy) | resource |
| [aws_ecr_repository.docker_image](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ecr_repository) | resource |
| [time_sleep.wait](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_image_tag_mutability"></a> [image\_tag\_mutability](#input\_image\_tag\_mutability) | used to set an ecr repo mutable or IMMUTABLE | `string` | `"MUTABLE"` | no |
| <a name="input_name"></a> [name](#input\_name) | name of ecr repository | `any` | n/a | yes |
| <a name="input_policy"></a> [policy](#input\_policy) | policy used for image retention | `string` | `"{\n    \"rules\": [\n        {\n            \"rulePriority\": 1,\n            \"description\": \"Keep last 3 images\",\n            \"selection\": {\n                \"tagStatus\": \"untagged\",\n                \"countType\": \"imageCountMoreThan\",\n                \"countNumber\": 3\n            },\n            \"action\": {\n                \"type\": \"expire\"\n            }\n        }    \n      ]\n}\n"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | name of not default tags passed | `map` | `{}` | no |
| <a name="input_time_sleep"></a> [time\_sleep](#input\_time\_sleep) | variable to let system sleep for a while before proceeding further, used that in some cases e.s. 'demanda platform' since permissions are tags oriented and aws needs some seconds before propagate them to avoid errors | `string` | `"0s"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_name"></a> [name](#output\_name) | returning name |
