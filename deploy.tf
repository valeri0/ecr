resource "aws_ecr_repository" "docker_image" {
  for_each             = toset(var.name)
  force_delete         = var.force_delete
  name                 = each.value
  tags                 = var.tags
  image_tag_mutability = var.image_tag_mutability
  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }
}
resource "aws_ecr_lifecycle_policy" "policy" {
  for_each   = toset(var.name)
  repository = aws_ecr_repository.docker_image[each.value].name
  policy     = var.policy
}

