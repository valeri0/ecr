variable "force_delete" {
  default     = true
  description = "if cancel repo also if containing images"
  type        = bool
}
variable "name" {
  description = "name of ecr repository"
  type        = any
}
variable "scan_on_push" {
  default     = true
  description = "if scan or not for vulnerabilities"
  type        = bool
}
variable "image_tag_mutability" {
  default     = "MUTABLE"
  description = "used to set an ecr repo mutable or IMMUTABLE"
  type        = string
}
variable "policy" {

  default     = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Keep last 3 images",
            "selection": {
                "tagStatus": "untagged",
                "countType": "imageCountMoreThan",
                "countNumber": 3
            },
            "action": {
                "type": "expire"
            }
        }    
      ]
}
EOF
  description = "policy used for image retention"
  type        = string
}
variable "tags" {
  default     = {}
  description = "name of not default tags passed"
  type        = map(any)
}
